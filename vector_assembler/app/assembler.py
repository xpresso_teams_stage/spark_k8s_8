
import os

from pyspark.ml.feature import VectorAssembler
from one_hot_encoder.app.abstract_component import AbstractSparkPipelineTransformer


class CustomVectorAssembler(VectorAssembler, AbstractSparkPipelineTransformer): 

    def __init__(self, name, run_id, inputCols=None, outputCol=None):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={self.name} with run_id {run_id}', flush=True)
        VectorAssembler.__init__(self, inputCols=inputCols, outputCol=outputCol)
        AbstractSparkPipelineTransformer.__init__(self, run_id)
    
    def _transform(self, dataset):
        self.state = dataset
        args = [self.run_id]
        should_run = self.start(*args)
        print(f"In {self.__class__.__name__} _transform, now calling super's _transform", flush=True)
        ds = super()._transform(dataset)
        self.state = ds
        self.completed()
        print(f'Completed component: {self.name}', flush=True)
        return ds