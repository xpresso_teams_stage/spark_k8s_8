from pymongo import MongoClient
from pymongo.database import Database
from pymongo.errors import ConnectionFailure
from pymongo.errors import PyMongoError
from pymongo.errors import DuplicateKeyError

import time
from enum import Enum, unique


# enumeration class for database operations
@unique
class DBOperation(Enum):
    INSERT = 1
    UPDATE = 2
    REPLACE = 3
    FIND = 4
    DELETE = 5

class MongoPersistenceManager:
    """
    This class performs various database (CRUD) operations on a Mongo DB database.
    Attributes:
        url (str) - URL of the api_server
        persistence (str) - name of the database
        uid (str) - user id
        pwd (str) - password
        w (int) - write concern (default = 0)
    """

    # (static) database connection
    mongo_db = None

    def __init__(self):
        config = {
            "mongodb": {
            "mongo_url":"mongodb://172.16.3.1:27017/?replicaSet=rs0",
            "local_mongo_url":"mongodb://localhost:27017/?replicaSet=rs0",
            "database":"xprdb",
            "mongo_uid":"xprdb_admin",
            "mongo_pwd":"xprdb@Abz00ba",
            "w":1,
            "interval_between_retries":5,
            "max_retries":2,
            "experiment_collection" : "experiments",
            "run_collection" : "runs"
            }
        }
        # persistence_manager = MongoPersistenceManager()
        # persistence_manager.connect()

        MONGOURL = config['mongodb']['mongo_url']
        MONGODB = config['mongodb']['database']
        MONGOUID = config['mongodb']['mongo_uid']
        MONGOPWD = config['mongodb']['mongo_pwd']
        self.INTERVAL_BETWEEN_RETRIES = int(config["mongodb"]["interval_between_retries"])
        self.MAX_RETRIES = int(config["mongodb"]["max_retries"])

        self.url = MONGOURL
        self.db = MONGODB
        self.uid = MONGOUID
        if len(self.uid) == 0:
            self.uid = None
        self.pwd =  MONGOPWD
        self.w = 1

    def connect(self) -> Database:
        if self.mongo_db is None:
            print(
                "Attempting connection to database %s on api_server %s" % (
                    self.db, self.url))
            # connect to api_server
            mongo_client = MongoClient(host=self.url, w=self.w)
            print("Created Mongo client object")

            connected = False
            attempt = 1
            while not connected and attempt <= self.MAX_RETRIES:
                try:
                    # The ismaster command is cheap and does not require auth.
                    print("Checking connection to api_server")
                    mongo_client.admin.command('ismaster')
                    connected = True
                    print("Connected to api_server successfully")
                except ConnectionFailure:
                    # TBD: LOG
                    print(
                        "Server not available: waiting for connection. Attempt %s of %s"
                        % (attempt, self.MAX_RETRIES))
                    # wait INTERVAL_BETWEEN_RETRIES seconds and retry MAX_RETRIES times
                    time.sleep(self.INTERVAL_BETWEEN_RETRIES)
                    attempt += 1
            if not connected:
                print(
                    "Unable to connect to database after %s attempts" % self.MAX_RETRIES)

            print("Connected to api_server successfully")
            # get database pointer
            print("Connecting to database %s" % self.db)
            MongoPersistenceManager.mongo_db = mongo_client[self.db]
            if MongoPersistenceManager.mongo_db is None:
                # TBD: LOG
                pass

            print(
                "Connected to database successfully. Authenticating user")
            # authenticate user
            try:
                if (self.uid is not None) and (self.pwd is not None):
                    MongoPersistenceManager.mongo_db.authenticate(self.uid,
                                                                  self.pwd)
            except PyMongoError:
                print(("Invalid user ID %s or password" % self.uid))

            # return database pointer
            print("Authentication successful")
        else:
            print("Connection already active")
        print(
            "Exiting connect method with return value %s" % MongoPersistenceManager.mongo_db)
        return MongoPersistenceManager.mongo_db

    def disconnect(self, mongo_client: MongoClient):
        """
        disconnects from the database
        """
        print(
            "Entering disconnect method with parameters %s" % mongo_client)
        try:
            # close connection
            # TBD: LOG
            mongo_client.close()
        except ConnectionFailure:
            # do nothing - no point throwing exception if problem closing connection
            print(
                "Connection failure while trying to disconnect from %s, %s" % (
                    self.url, self.db))
            pass
        print("Disconnected sucessfully")
        print("Exiting disconnect method")

    def update(self, collection: str, doc_filter, obj, upsert: bool = False):
        """

        :param collection: (string) name of collection to update
        :param doc_filter: filter for the object to be updated
        :param obj: new attributes of the object to be updated
        :param upsert: (bool) true if object to be inserted if not found
        :return: ID of the object updated
        """
        print(
            "Entering update method with parameters collection: %s, doc_filter: %s, obj: %s, upsert: %s"
            % (collection, doc_filter, obj, upsert))
        kwargs = {}
        kwargs["filter"] = doc_filter
        update = {}
        update["$set"] = obj
        kwargs["update"] = update
        kwargs["upsert"] = upsert
        print("Calling perform_db_operation for update operation")
        result = self.perform_db_operation(collection, DBOperation.UPDATE,
                                           **kwargs)
        doc_id = result.upserted_id
        print("Update successful. Document ID: %s" % doc_id)

        print("Exiting update method with return value %s" % doc_id)
        return doc_id

    def find(self, collection: str, doc_filter):
        """
        finds one or more documents in the collection matching the specified filter
        :param collection: (str)  to be searched
        :param doc_filter: (dict) query to be applied
        :return: (array of dict) document(s) found, or None
        """
        print(
            "Entering fnd method with parameters collection: %s, doc_filter: %s"
            % (collection, doc_filter))
        kwargs = {"filter": doc_filter}
        print("Calling perform_db_operation for find operation")
        result = self.perform_db_operation(collection, DBOperation.FIND,
                                           **kwargs)
        print(
            "Operation completed. Results: %s. Converting to object array" % result)

        # convert result from cursor to array of dict
        final_res = []

        for record in result:
            final_res.append(record)

        result.close()
        print("Conversion complete. Results: %s" % final_res)
        print("Exiting find method with return value %s" % final_res)
        return final_res


    def perform_db_operation(self, collection: str, operation: str, **kwargs):
        """
        performs a database operation - guarantees success within N retries (throws UnsuccessfulOperationException
        if unsuccessful after N retries)
        :param collection: (str) name of collection to be operated on
        :param operation: (str) name of operation to be performed
        :param kwargs: arguments for operation
        :return:
        """
        print(
            "Entering perform_db_operation method with parameters collection: %s, operation: %s, "
            "arguments: %s" % (collection, operation, kwargs))
        # connect to the database
        print("Connecting to database")
        mongo_db = self.connect()
        mongo_client = mongo_db.client
        print("Connected to database")

        # get the collection pointer - throw exception if not found
        mongo_collection = mongo_db[collection]
        if mongo_collection is None:
            print('UnsuccessfulOperationException')
            return
        print("Got collection %s" % collection)

        # try the operation - repeat MAX_RETRIES times
        operation_successful = False
        attempt = 1
        result = None
        while not operation_successful and attempt <= self.MAX_RETRIES:
            print("Attempting %s operation. Attempt %s of %s" % (
                operation, attempt, self.MAX_RETRIES))
            try:
                # perform operation
                if operation is DBOperation.INSERT:
                    print("attempting mongo_collection.insert_one")
                    try:
                        result = mongo_collection.insert_one(kwargs)
                        operation_successful = result.acknowledged
                        print(
                            "Attempt complete. Result = %s" % operation_successful)
                    except DuplicateKeyError:
                        print('Duplicate Key Error')
                        operation_successful = False
                        print('PrimaryKeyException')
                        return
                elif operation is DBOperation.FIND:
                    print("attempting mongo_collection.find")
                    result = mongo_collection.find(**kwargs)
                    operation_successful = True
                    print(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.UPDATE:
                    print("attempting mongo_collection.update_one")
                    result = mongo_collection.update_one(**kwargs)
                    operation_successful = result.acknowledged
                    print(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.REPLACE:
                    print("attempting mongo_collection.replace_one")
                    result = mongo_collection.replace_one(**kwargs)
                    operation_successful = result.acknowledged
                    print(
                        "Attempt complete. Result = %s" % operation_successful)
                elif operation is DBOperation.DELETE:
                    print("attempting mongo_collection.delete_many")
                    result = mongo_collection.delete_many(**kwargs)
                    operation_successful = result.acknowledged
                    print(
                        "Attempt complete. Result = %s" % operation_successful)
            except ConnectionFailure:
                # try again after INTERVAL_BETWEEN_RETRIES seconds
                print(
                    "Connection Failure. Trying again after %s seconds" % self.INTERVAL_BETWEEN_RETRIES)
                time.sleep(self.INTERVAL_BETWEEN_RETRIES)
                attempt += 1

        # disconnect from database
        print("Disconnecting from database")
        # self.disconnect(mongo_client)
        print("Disconnected successfully")

        # operation may not have succeeded even after MAX_RETRIES attempts
        if not operation_successful:
            print(
                "Operation unsuccessful even after %s attempts" % self.MAX_RETRIES)

        print(
            "Exiting from perform_db_operation method with return value %s" % result)
        return result